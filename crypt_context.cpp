#include "crypt_context.hpp"

#ifdef _WIN32
crypt_context::crypt_context() {
    CryptAcquireContextW(&m_context, NULL, MS_ENH_RSA_AES_PROV, PROV_RSA_AES,
                         CRYPT_VERIFYCONTEXT);
}
#endif
