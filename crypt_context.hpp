#ifndef CRYPT_CONTEXT_HPP
#define CRYPT_CONTEXT_HPP

#ifdef _WIN32
#include <windows.h>

class crypt_context {
  public:
    crypt_context();
    ~crypt_context() { CryptReleaseContext(m_context, 0); }
    HCRYPTPROV get() const { return m_context; }

  private:
    HCRYPTPROV m_context;
};
#endif // _WIN32

#endif // CRYPT_CONTEXT_HPP
