#ifndef CRYPT_ERROR_HPP
#define CRYPT_ERROR_HPP

#include <windows.h>

#include <exception>
#include <sstream>

#ifdef _WIN32
class crypt_error : public std::exception {
  public:
    crypt_error() : error(GetLastError()) {
        std::stringstream error_msg;
        error_msg << "Crypt error: 0x" << std::hex << error;
        char *format_string = get_message();
        if (format_string) {
            error_msg << "\n" << format_string;
            LocalFree(format_string);
        }
        error_string = error_msg.str();
    }
    virtual ~crypt_error() {}
    virtual const char *what() const { return error_string.c_str(); }

  private:
    char *get_message() {
        char *result = nullptr;
        FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER |
                           FORMAT_MESSAGE_FROM_SYSTEM |
                           FORMAT_MESSAGE_IGNORE_INSERTS,
                       NULL, error, LANG_USER_DEFAULT,
                       reinterpret_cast<char *>(&result), 0, NULL);
        return result;
    }
    DWORD error;
    std::string error_string;
};
#elif defined(__APPLE__) && defined(__MACH__)
class crypt_error : public std::exception {};
#endif

#endif // CRYPT_ERROR_HPP
