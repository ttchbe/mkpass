#include "hash.hpp"

#include <cctype>
#include <iomanip>
#include <sstream>
#include <string>

std::string to_string(const std::vector<char> &bytes, size_t maxlen) {
    std::ostringstream o;
    for (auto iter = begin(bytes); iter != begin(bytes) + (maxlen / 2);
         ++iter) {
        o << std::setw(2) << std::setfill('0') << std::hex
          << (0xff & static_cast<int>(*iter));
    }
    return o.str();
}

char get_symbol(const char c) {
    switch (c) {
    case '0':
        return ')';
    case '1':
        return '!';
    case '2':
        return '@';
    case '3':
        return '#';
    case '4':
        return '$';
    case '5':
        return '%';
    case '6':
        return '^';
    case '7':
        return '&';
    case '8':
        return '*';
    case '9':
        return '(';
    default:
        return ' ';
    }
}

char get_number(const char c) {
    switch (c) {
    case ')':
        return '0';
    case '!':
        return '1';
    case '@':
        return '2';
    case '#':
        return '3';
    case '$':
        return '4';
    case '%':
        return '5';
    case '^':
        return '6';
    case '&':
        return '7';
    case '*':
        return '8';
    case '(':
        return '9';
    default:
        return ' ';
    }
}

std::string add_complexity(std::string s) {
    size_t offset = s.find_first_not_of("1234567890");
    if (offset != std::string::npos) {
        auto it = begin(s) + offset;
        *it = static_cast<char>(std::toupper(*it));
        offset = s.find_last_of("1234567890");
        if (offset != std::string::npos) {
            it = begin(s) + offset;
            *it = get_symbol(*it);
        }
    }
    return s;
}

std::string remove_complexity(std::string s) {
    auto offset = s.find_first_not_of("!@#$%^&*()1234567890");
    if (offset != std::string::npos) {
        auto it = begin(s) + offset;
        *it = static_cast<char>(std::tolower(*it));
        offset = s.find_last_of("!@#$%^&*()");
        if (offset != std::string::npos) {
            it = begin(s) + offset;
            *it = get_number(*it);
        }
    }
    return s;
}
