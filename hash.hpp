#ifndef HASH_HPP
#define HASH_HPP

#include <memory>
#include <string>
#include <vector>

class CommonHash {
  public:
    virtual void add_char(const char) = 0;
    virtual void add_char(const wchar_t) = 0;
    virtual std::vector<char> get_hash() const = 0;
    virtual std::shared_ptr<CommonHash> clone() const = 0;
    virtual ~CommonHash() {}
};

#if defined(__APPLE__) && defined(__MACH__)
#include "osx/hash.hpp"
#elif defined(_WIN32)
#include "win32/hash.hpp"
#endif

std::string to_string(const std::vector<char> &, size_t);
std::string add_complexity(std::string);
std::string remove_complexity(std::string);

#endif // HASH_HPP
