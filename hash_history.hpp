#ifndef HASH_HISTORY_HPP
#define HASH_HISTORY_HPP

#include <memory>
#include <vector>

#include "hash.hpp"

template <typename Hash> class hash_history {
  public:
    void add(CommonHash *h) { m_history.push_back(h->clone()); }
    std::shared_ptr<CommonHash> undo() {
        m_history.pop_back();
        return m_history.back()->clone();
    }
    std::shared_ptr<CommonHash> current() {
        if (m_history.empty()) {
            //      m_history.push_back(std::make_shared<Hash>());
            return nullptr;
        }
        return m_history.back()->clone();
    }
    void clear() { m_history.clear(); }

  private:
    std::vector<std::shared_ptr<CommonHash> > m_history;
};

#endif // HASH_HISTORY_HPP
