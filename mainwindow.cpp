#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QClipboard>
#include <QKeyEvent>
#include <QLayout>
#include <QMessageBox>
#include <QTimer>

#include <memory>
#include <string>

#include "hash_history.hpp"
#include "hash.hpp"

const int MAXIMUM_OUTPUT_LENGTH = 16;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow), last_text_size(0),
      timer(this) {
    ui->setupUi(this);
    layout()->setSizeConstraint(QLayout::SetFixedSize);
    ui->maximum_length->setMaximum(MAXIMUM_OUTPUT_LENGTH);
    ui->maximum_length->setValue(MAXIMUM_OUTPUT_LENGTH);
    connect(&timer, SIGNAL(timeout()), this, SLOT(empty_clipboard()));
    ui->password->installEventFilter(this);
    ui->complexity->installEventFilter(this);
    ui->show_output->installEventFilter(this);
    ui->maximum_length->installEventFilter(this);
    ui->output->installEventFilter(this);
    set_output();
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::reset() {
    output_.clear();
    last_text_size = 0;
    m_history.clear();
    ui->password->clear();
    ui->output->clear();
    ui->password->setFocus();
}

void MainWindow::on_password_textEdited(const QString &arg1) {
    try {
        timer.stop();
        auto text = arg1;
        std::shared_ptr<CommonHash> hash; // = std::make_shared<sha256_hash>();
        if (text.size() == 0) {
            ui->output->clear();
            m_history.clear();
            return;
        } else if (last_text_size > text.size()) { // backspace was pressed
            hash = m_history.undo();
        } else {
            hash = m_history.current();
            if (!hash) {
                hash = std::make_shared<sha256_hash>();
            }
            hash->add_char(text.toStdWString().back());
            m_history.add(hash.get());
        }
        output_ = to_string(hash->get_hash(), MAXIMUM_OUTPUT_LENGTH);
        set_output();
        last_text_size = text.size();
        ui->password->setText(QString(text.size(), '#'));
    }
    catch (const std::exception &e) {
        QMessageBox::critical(this, "Unhandled exception",
                              QString::fromStdString(e.what()));
    }
}

void MainWindow::on_complexity_toggled(bool checked) { set_output(); }

void MainWindow::on_show_output_toggled(bool checked) { set_output(); }

void MainWindow::on_maximum_length_valueChanged(int) { set_output(); }

void MainWindow::on_password_cursorPositionChanged() {
    ui->password->setCursorPosition(ui->password->text().size());
}

void MainWindow::empty_clipboard() {
    QApplication::clipboard()->clear();
    timer.stop();
}

void MainWindow::set_output() {
    bool output_enabled = ui->show_output->isChecked();
    ui->output->setEnabled(output_enabled);
    if (!output_enabled) {
        ui->output->clear();
    } else {
        ui->output->setText(get_password());
    }
}

void MainWindow::finalize_password() {
    QApplication::clipboard()->setText(get_password());
    reset();
    const int password_timeout_ms = 10000;
    timer.start(password_timeout_ms);
}

QString MainWindow::get_password() {
    auto length = ui->maximum_length->value();
    return QString::fromStdString(
        ui->complexity->isChecked() ? add_complexity(output_.substr(0, length))
                                    : output_.substr(0, length));
}

bool MainWindow::eventFilter(QObject *watched, QEvent *e) {
    if (e->type() == QEvent::KeyPress) {
        QKeyEvent *ke = static_cast<QKeyEvent *>(e);
        if (ke->key() == Qt::Key_Enter || ke->key() == Qt::Key_Return) {
            finalize_password();
            return true;
        }
        if (ke->key() == Qt::Key_Escape) {
            reset();
            return true;
        }
    }
    return QWidget::eventFilter(watched, e);
}
