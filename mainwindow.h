#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <string>
#include <vector>

#include "hash.hpp"
#include "hash_history.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

  public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

  private
slots:
    void on_complexity_toggled(bool checked);

    void on_show_output_toggled(bool checked);

    void on_password_cursorPositionChanged();

    void on_password_textEdited(const QString &arg1);

    void on_maximum_length_valueChanged(int i);

    void empty_clipboard();

    bool eventFilter(QObject *watched, QEvent *e);

  private:
    void reset();

    void finalize_password();

    void set_output();

    QString get_password();

    Ui::MainWindow *ui;
    hash_history<sha256_hash> m_history;
    decltype(QString().size()) last_text_size;
    QTimer timer;
    std::string output_;
};

#endif // MAINWINDOW_H
