#define UNICODE
#include <windows.h>
#include <assert.h>
#include <conio.h>
#include <stdio.h>

#include <algorithm>
#include <cctype>
#include <codecvt>
#include <exception>
#include <future>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <locale>
#include <sstream>
#include <string>
#include <vector>

class crypt_error : public std::exception {
  public:
    crypt_error() : error(GetLastError()) {
        std::stringstream error_msg;
        error_msg << "Crypt error: 0x" << std::hex << error;
        char *format_string = get_message();
        if (format_string) {
            error_msg << "\n" << format_string;
            LocalFree(format_string);
        }
        error_string = error_msg.str();
    }
    virtual ~crypt_error() {}
    virtual const char *what() const { return error_string.c_str(); }

  private:
    char *get_message() {
        char *result = nullptr;
        FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER |
                           FORMAT_MESSAGE_FROM_SYSTEM |
                           FORMAT_MESSAGE_IGNORE_INSERTS,
                       NULL, error, LANG_USER_DEFAULT,
                       reinterpret_cast<char *>(&result), 0, NULL);
        return result;
    }
    DWORD error;
    std::string error_string;
};

class console_hwnd {
  public:
    console_hwnd() : m_hwnd(NULL) {
        std::wstring title(64 * 1024, '\0');
        GetConsoleTitleW(&title[0], static_cast<DWORD>(title.size()));
        SetConsoleTitleA("MKPASS0000");
        Sleep(40);
        m_hwnd = FindWindowA(NULL, "MKPASS0000");
        SetConsoleTitleW(title.c_str());
    }
    HWND get() const { return m_hwnd; }

  private:
    HWND m_hwnd;
};

class clipboard {
  public:
    explicit clipboard(HWND owner) {
        if (!OpenClipboard(owner)) {
            throw std::runtime_error("could not open clipboard");
        }
    }
    clipboard() {
        console_hwnd ch;
        if (!OpenClipboard(ch.get())) {
            throw std::runtime_error("could not open clipboard");
        }
    }
    ~clipboard() { CloseClipboard(); }
    void set_text(const std::string &text) {
        EmptyClipboard();
        HGLOBAL mem = GlobalAlloc(GMEM_MOVEABLE, text.size() + 1);
        char *text_copy = reinterpret_cast<char *>(GlobalLock(mem));
        memcpy(text_copy, text.c_str(), text.size());
        text_copy[text.size()] = 0;
        GlobalUnlock(mem);
        SetClipboardData(CF_TEXT, mem);
    }

  private:
};

template <DWORD Hash> class hash {
  public:
    hash() {
        check_error(CryptAcquireContextW(&provider, NULL, MS_ENH_RSA_AES_PROV,
                                         PROV_RSA_AES, CRYPT_VERIFYCONTEXT));
        check_error(CryptCreateHash(provider, Hash, 0, 0, &os_hash));
    }
    ~hash() { CryptDestroyHash(os_hash); }
    void hash_data(const std::vector<char> &in) const {
        std::vector<char> copy(in.size());
        std::copy(begin(in), end(in), begin(copy));
        check_error(CryptHashData(os_hash, reinterpret_cast<BYTE *>(&copy[0]),
                                  static_cast<DWORD>(in.size()), 0));
    }
    void add_char(const char c) const {
        BYTE b = c;
        check_error(CryptHashData(os_hash, &b, 1, 0));
    }
    std::vector<char> get_hash() const {
        DWORD data_size = get_hash_size();
        std::vector<char> data(data_size);
        check_error(CryptGetHashParam(os_hash, HP_HASHVAL,
                                      reinterpret_cast<BYTE *>(data.data()),
                                      &data_size, 0));
        return data;
    }
    template <typename CharT>
    void hash_string(const std::basic_string<CharT> &in) const {
        std::vector<char> vec(in.size() * sizeof(CharT));
        std::copy(begin(in), end(in), begin(vec));
        hash_data(vec);
    }

  private:
    void check_error(BOOL crypt_succeeded) const {
        if (!crypt_succeeded) {
            throw crypt_error();
        }
    }
    DWORD get_hash_size() const {
        DWORD hash_size;
        DWORD data_size = sizeof(DWORD);
        check_error(CryptGetHashParam(os_hash, HP_HASHSIZE,
                                      reinterpret_cast<BYTE *>(&hash_size),
                                      &data_size, 0));
        assert(data_size == sizeof(DWORD));
        if (data_size != sizeof(DWORD)) {
            throw std::runtime_error("CryptGetHashParam: unexpected data size");
        }
        return hash_size;
    }
    HCRYPTPROV provider;
    HCRYPTHASH os_hash;
};
typedef hash<CALG_SHA_256> sha256_hash;
typedef hash<CALG_SHA_384> sha384_hash;
typedef hash<CALG_SHA_512> sha512_hash;

std::string to_string(const std::vector<char> &bytes, size_t maxlen) {
    std::ostringstream o;
    for (auto iter = begin(bytes); iter != begin(bytes) + (maxlen / 2);
         ++iter) {
        o << std::setw(2) << std::setfill('0') << std::hex
          << (0xff & static_cast<int>(*iter));
    }
    return o.str();
}

template <typename Hash> void input_password(Hash *h) {
    int c = _getch();
    while (c != '\r') {
        if (!std::isprint(c)) {
            throw std::runtime_error(
                "invalid input (characters must be printable)");
        }
        h->add_char(static_cast<char>(c));
        c = _getch();
    }
}

template <typename Hash> std::vector<char> get_password() {
    Hash p1;
    Hash p2;
    std::cout << "Enter password: ";
    input_password(&p1);
    std::cout << "\n";
    std::cout << "Confirm password: ";
    input_password(&p2);
    std::cout << "\n";
    auto h1 = p1.get_hash();
    auto h2 = p2.get_hash();
    if (std::equal(begin(h1), end(h1), begin(h2))) {
        return h1;
    } else {
        return std::vector<char>();
    }
}

char get_symbol(const char c) {
    switch (c) {
    case '0':
        return ')';
    case '1':
        return '!';
    case '2':
        return '@';
    case '3':
        return '#';
    case '4':
        return '$';
    case '5':
        return '%';
    case '6':
        return '^';
    case '7':
        return '&';
    case '8':
        return '*';
    case '9':
        return '(';
    default:
        return ' ';
    }
}

std::string add_complexity(std::string s) {
    size_t offset = s.find_first_not_of("1234567890");
    auto it = begin(s) + offset;
    *it = static_cast<char>(std::toupper(*it));
    offset = s.find_last_of("1234567890");
    it = begin(s) + offset;
    *it = get_symbol(*it);
    return s;
}

void add_to_clipboard(const std::string &s) {
    clipboard c;
    c.set_text(s);
}

int wmain(int argc, wchar_t *argv[]) {
    try {
        decltype(get_password<sha256_hash>()) output;
        if (argc == 1) {
            output = get_password<sha256_hash>();
        } else {
            sha256_hash hash;
            std::wstring_convert<std::codecvt_utf8<wchar_t> > conv;
            hash.hash_string(conv.to_bytes(argv[1]));
            output = hash.get_hash();
        }
        if (output.size() > 15) {
            add_to_clipboard(to_string(output, 16));
            std::cout << to_string(output, 16) << "\n";
            std::cout << add_complexity(to_string(output, 16)) << "\n";
        }
    }
    catch (const std::exception &e) {
        std::cout << e.what() << "\n";
    }
}
