#ifndef OSX_HASH_HPP
#define OSX_HASH_HPP

#include <CommonCrypto/CommonDigest.h>

#include <cstdlib>
#include <memory>
#include <vector>

#include "../hash.hpp"

namespace hash {
namespace osx {

template <typename Context> class hash_ : public CommonHash {
  public:
    hash_() : CommonHash() {}
    virtual ~hash_() {}
    hash_(const hash_ &rhs) : CommonHash(rhs) { *this = rhs; }
    hash_ &operator=(const hash_ &rhs) {
        m_ctx = std::make_shared<Context>(*rhs.m_ctx);
        return *this;
    }
    void add_char(char c) override {
        init_if_needed();
        update(m_ctx.get(), &c, 1);
    }
    void add_char(wchar_t c) override {
        init_if_needed();
        std::vector<char> mb(MB_CUR_MAX);
        mb.resize(std::wctomb(mb.data(), c));
        update(m_ctx.get(), mb.data(), mb.size());
    }
    std::vector<char> get_hash() const override {
        std::vector<char> result(digest_length(m_ctx.get()));
        final(reinterpret_cast<unsigned char *>(result.data()), m_ctx.get());
        return result;
    }
    std::shared_ptr<CommonHash> clone() const {
        return std::make_shared<hash_<Context> >(*this);
    }

  private:
    void init_if_needed() {
        if (!m_ctx) {
            m_ctx.reset(new Context());
            init(m_ctx.get());
        }
    }
    int init(Context *ctx) { return CC_SHA256_Init(ctx); }
    int update(Context *ctx, const void *data, CC_LONG len) const {
        return CC_SHA256_Update(ctx, data, len);
    }
    int final(unsigned char *md, Context *ctx) const {
        return CC_SHA256_Final(md, ctx);
    }
    size_t digest_length(const Context *ctx) const {
        return CC_SHA256_DIGEST_LENGTH;
    }
    std::shared_ptr<Context> m_ctx;
};

} // namespace osx
} // namespace hash

typedef hash::osx::hash_<CC_SHA256_CTX> sha256_hash;
typedef hash::osx::hash_<CC_SHA512_CTX> sha512_hash;

#endif // OSX_HASH_HPP
