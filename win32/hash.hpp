#ifndef WIN32_HASH_HPP
#define WIN32_HASH_HPP

#include <windows.h>

#include <cassert>

#include "../hash.hpp"
#include "crypt_context.hpp"
#include "crypt_error.hpp"

namespace hash {
namespace win32 {

void check_error(BOOL);

template <DWORD Hash> class hash_ : public CommonHash {
  public:
    hash_()
        : m_provider(std::make_shared<Provider>()),
          os_hash(reinterpret_cast<HCRYPTHASH>(INVALID_HANDLE_VALUE)),
          contains_data(false), retrieved(false) {}
    virtual ~hash_() {
        if (os_hash != reinterpret_cast<HCRYPTHASH>(INVALID_HANDLE_VALUE)) {
            CryptDestroyHash(os_hash);
        }
    }
    hash_(const hash_ &rhs) : CommonHash(rhs) { *this = rhs; }
    hash_ &operator=(const hash_ &rhs) {
        m_provider = rhs.m_provider;
        os_hash = reinterpret_cast<HCRYPTHASH>(INVALID_HANDLE_VALUE);
        if (rhs.contains_data) {
            check_error(CryptDuplicateHash(rhs.os_hash, 0, 0, &os_hash));
        }
        contains_data = rhs.contains_data;
        retrieved = rhs.retrieved;
        return *this;
    }
    hash_(hash_ &&rhs) : CommonHash(rhs) { *this = std::move(rhs); }
    hash_ &operator=(hash_ &&rhs) {
        m_provider = std::move(rhs.m_provider);
        os_hash = std::move(rhs.os_hash);
        contains_data = std::move(rhs.contains_data);
        retrieved = std::move(rhs.retrieved);
        std::swap(m_provider, rhs.m_provider);
        rhs.os_hash = reinterpret_cast<HCRYPTHASH>(INVALID_HANDLE_VALUE);
        rhs.contains_data = false;
        rhs.retrieved = false;
        return *this;
    }
    std::shared_ptr<CommonHash> clone() const {
        return std::make_shared<hash_<Hash> >(*this);
    }
    void hash_data(const std::vector<char> &in) {
        std::vector<char> copy(in.size());
        std::copy(begin(in), end(in), begin(copy));
        if (os_hash == reinterpret_cast<HCRYPTHASH>(INVALID_HANDLE_VALUE)) {
            check_error(
                CryptCreateHash(m_provider->get(), Hash, 0, 0, &os_hash));
        }
        assert(retrieved == false);
        check_error(CryptHashData(os_hash, reinterpret_cast<BYTE *>(&copy[0]),
                                  static_cast<DWORD>(copy.size()), 0));
        contains_data = true;
    }
    void add_char(const char c) {
        std::vector<char> vec;
        vec.push_back(c);
        hash_data(vec);
    }
    void add_char(const wchar_t c) {
        std::string mb(MB_CUR_MAX, '\0');
        std::wctomb(&mb[0], c);
        std::vector<char> vec;
        std::copy(begin(mb), end(mb), std::back_inserter(vec));
        hash_data(vec);
    }
    std::vector<char> get_hash() const {
        if (!contains_data)
            return std::vector<char>();
        DWORD data_size = get_hash_size();
        std::vector<char> data(data_size);
        check_error(CryptGetHashParam(os_hash, HP_HASHVAL,
                                      reinterpret_cast<BYTE *>(data.data()),
                                      &data_size, 0));
        retrieved = true;
        return data;
    }
    void hash_string(const std::wstring &in) {
        std::vector<char> vec;
        std::for_each(begin(in), end(in), [&vec](wchar_t c) {
            std::string mb(MB_CUR_MAX, '\0');
            std::wctomb(&mb[0], c);
            std::copy(begin(mb), end(mb), std::back_inserter(vec));
        });
        hash_data(vec);
    }
    void hash_string(const std::string &in) {
        std::vector<char> vec(in.size());
        std::copy(begin(in), end(in), begin(vec));
        hash_data(vec);
    }

  private:
    DWORD get_hash_size() const {
        if (!contains_data)
            return 0;
        DWORD hash_size;
        DWORD data_size = sizeof(DWORD);
        check_error(CryptGetHashParam(os_hash, HP_HASHSIZE,
                                      reinterpret_cast<BYTE *>(&hash_size),
                                      &data_size, 0));
        assert(data_size == sizeof(DWORD));
        if (data_size != sizeof(DWORD)) {
            throw std::runtime_error("CryptGetHashParam: unexpected data size");
        }
        return hash_size;
    }
    class Provider {
      public:
        Provider() {
            check_error(CryptAcquireContextW(&m_provider, NULL,
                                             MS_ENH_RSA_AES_PROV, PROV_RSA_AES,
                                             CRYPT_VERIFYCONTEXT));
        }
        ~Provider() { CryptReleaseContext(m_provider, 0); }
        HCRYPTPROV get() const { return m_provider; }

      private:
        HCRYPTPROV m_provider;
    };
    std::shared_ptr<Provider> m_provider;
    HCRYPTHASH os_hash;
    bool contains_data;
    mutable bool retrieved;
};

} // namespace win32
} // namespace hash

typedef hash::win32::hash_<CALG_SHA_256> sha256_hash;
typedef hash::win32::hash_<CALG_SHA_384> sha384_hash;
typedef hash::win32::hash_<CALG_SHA_512> sha512_hash;

#endif // WIN32_HASH_HPP
